#!/usr/bin/env groovy

// COMPLETE CI/CD PIPELINE USING DOCKER COMPOSE TO DEPLOY ON EC2 REMOTE SERVER

// This pipeline builds the Java Maven app from the Module 8 demos.
// Made Module 8 demos DockerHub repo private for this pipeline build.

// This import method means the JSL is project-scoped (NOT globally available to all Jenkins pipelines)
// updated to use own GL repo URL and Jenkins-configured GL creds
library identifier: 'jenkins-shared-library-aws@main', retriever: modernSCM(
    [$class: 'GitSCMSource',
    remote: 'https://gitlab.com/twndevops/jenkins-shared-library-aws.git',
    credentialsID: 'git-creds'
    ]
)

pipeline {
    agent any
    tools {
        maven 'maven-3.9.6'
    }
    environment {
        // hard coding image
        IMAGE_NAME = 'upnata/module-8-java-mvn-app:1.0-SNAPSHOT'
    }
    stages {
        stage('build app') {
            steps {
                buildJar()
            }
        }
        stage('build image') {
            steps {
                script {
                    // Groovy file is buildImage, lib func is buildDockerImage
                    buildImage(env.IMAGE_NAME)
                    // Jenkins authenticates to Docker to push img to private repo
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        }
        stage("provision server") {
            environment {
                AWS_ACCESS_KEY_ID = credentials("aws_access_key_global")
                AWS_SECRET_ACCESS_KEY = credentials("aws_secret_key_global")
                TF_VAR_env_prefix = "test"
            }
            steps {
                script {
                    echo 'Terraform provisioning EC2...'
                    // change curr dir to terraform so TF cmds work
                    // Reference: https://www.jenkins.io/doc/pipeline/steps/workflow-durable-task-step/#dir-change-current-directory
                    dir("terraform") {
                        sh "terraform init"
                        sh "terraform apply --auto-approve"
                        EC2_PUB_IP = sh(
                            script: "terraform output aws_instance_public_ip",
                            // prints IP value to std output to save to env var
                            returnStdout: true
                        ).trim() // trims unnecessary surrounding spaces
                    }
                }
            }
        }
        stage("deploy") {
            environment {
                DOCKER_CREDS = credentials('dockerhub-private-repo')
            }
            steps {
                script {
                    // allow time for EC2 initialization/ entrypt-script.sh to finish
                    echo 'awaiting EC2 initialization...'
                    sleep(time: 90, unit: "SECONDS")

                    echo 'composing up Docker network on EC2...'

                    def ec2endpt = "ec2-user@${EC2_PUB_IP}"    
                    def bashCmds = "bash ./ec2-cmds.sh ${IMAGE_NAME} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"

                    // use EC2 ssh private key credential scoped to java-mvn-tf-build
                    sshagent(['ec2-ssh-key']) {
                        // copy bash script file and Docker Compose YAML from Jenkins' locally checked out repo to EC2 server working dir (cmd `pwd`)
                        // need permissions to scp into remote EC2 (must run in sshagent block)
                        sh "scp -o StrictHostKeyChecking=no ec2-cmds.sh ${ec2endpt}:/home/ec2-user"
                        // running next cmd in bash script file fails b/c no longer on Jenkins server (which has Compose file); EC2 doesn't have any Compose file so the cmd doesn't work
                        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2endpt}:/home/ec2-user"
                        // use EC2 public IP
                        sh "ssh -o StrictHostKeyChecking=no ${ec2endpt} ${bashCmds}"
                    }
                }
            }               
        }
        // pull changes after every build!
    }
}