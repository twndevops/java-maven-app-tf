#!/bin/bash
sudo yum update -y
sudo yum install -y docker
sudo systemctl start docker
sudo chown ec2-user /var/run/docker.sock
sudo systemctl start docker
# Reference: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04, https://github.com/docker/compose/tags
sudo curl -L "https://github.com/docker/compose/releases/download/v2.24.0-birthday.10/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
