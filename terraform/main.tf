terraform {
  // Terraform will error if you use a more recent CLI version than 1.6.x
  // Run "terraform version" to get latest version
  // Reference: https://developer.hashicorp.com/terraform/tutorials/configuration-language/versions
  required_version = "~> 1.6.6"
  // remote state storage
  backend "s3" {
    // TF errors out on "init" if any of the following use vars
    // existing bucket's name
    bucket = "java-maven-app-tf-test-bucket"
    // path in bucket (which can have dir hierarchy, since S3 is for file storage)
    key = "state/java-maven-app-tf-test.tfstate"
    // bucket region does NOT need to match region containing infra provisioned below
    region = "us-east-1"
  }
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.az
  tags = {
    Name: "${var.env_prefix}-subnet-1"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }
}

# FOLLOWING creates new Internet Route in new default rtb for custom VPC created above, using IGW created above
resource "aws_default_route_table" "myapp-main-rtb" {
  # no need to supply vpc_id since default rtb exists in VPC already
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id # or main_route_table_id

  # in-VPC traffic handling Route already exists in main rtb

  route {
    # any IP (range)
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name: "${var.env_prefix}-main-rtb"
  }
}

resource "aws_default_security_group" "myapp-default-sg" {
  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    description = "SSH into EC2"
    from_port = 22
    to_port = 22
    protocol = "tcp"
    # set your IP to access port opened above
    cidr_blocks = [var.my_ip, var.jenkins_ip]
  }

  ingress {
    description = "Browser access"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    # any IP can access from browser
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "All outbound traffic to Internet"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amzn-linux-img" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "image-id"
    values = ["ami-079db87dc4c10ac91"]
  }
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amzn-linux-img.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  availability_zone = var.az

  vpc_security_group_ids = [aws_default_security_group.myapp-default-sg.id]

  # associate SSH key pair w/ EC2 being created (enables SSH access)
  key_name = "tf-jenkins-key-pair"

  associate_public_ip_address = true

  user_data = file("./entrypt-script.sh")

  tags = {
    Name = "${var.env_prefix}-server"
  }
}