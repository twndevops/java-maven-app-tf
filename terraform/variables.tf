variable "region" {
  default = "us-east-1"
}
variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}
variable "subnet_cidr_block" {
  default = "10.0.10.0/24"
}
variable "az" {
  default = "us-east-1d"
}
variable "env_prefix" {
  default = "dev"
}
variable "my_ip" {
  default = "172.56.71.198/32"
}
variable "jenkins_ip" {
  default = "159.65.220.254/32"
}
variable "instance_type" {
  default = "t2.micro"
}