#!/user/bin/env bash

# read in script run cmd params (IMAGE_NAME) using numbers of params
# export = store/ set as new env var on EC2 server
export LATEST_IMAGE=$1

# EC2 authenticates to Docker to pull img from private repo
echo $3 | docker login -u $2 --password-stdin

docker-compose up -d